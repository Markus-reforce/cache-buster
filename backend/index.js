const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const jwt = require("express-jwt");
const jwks = require("jwks-rsa");
const app = express();
const setupRoutes = require("./routes.js");
const initDb = require("./db");

const jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: "https://reforce.eu.auth0.com/.well-known/jwks.json"
  }),
  audience: "http://localhost:3000",
  issuer: "https://reforce.eu.auth0.com/",
  algorithms: ["RS256"]
});

app.use(cors());
app.use(jwtCheck);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

initDb()
  .then(db => {
    const server = setupRoutes(app, db).listen(
      process.env.PORT || 8080,
      function() {
        console.log("app running on port.", server.address().port);
      }
    );
  })
  .catch(err => {
    console.error("Failed to make all database connections!");
    console.error(err);
    process.exit(1);
  });
