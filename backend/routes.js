const findTenants = function(db) {
  const collection = db.collection("tenant");
  return collection.find({}).toArray();
};

const findTeams = (db, tenantId) => {
  return db
    .collection("team")
    .find({
      "tenantId.identifier": tenantId
    })
    .toArray();
};

const findTeamKpiTerms = (db, teamId) => {
  return db
    .collection("teamKpi")
    .aggregate([
      {
        $match: { "teamId.identifier": teamId, archived: false }
      },
      {
        $lookup: {
          from: "term",
          localField: "termId.identifier",
          foreignField: "termId.identifier",
          as: "terms"
        }
      },
      { $unwind: "$terms" },
      {
        $project: {
          _id: 0,
          teamKpiId: "$teamKpiId.identifier",
          name: "$terms.name",
          termId: "$termId.identifier",
          periodMetricUserAchievement: 1
        }
      }
    ])
    .toArray();
};

const clearTeamTermCache = async (db, periodMetricUserAchievementId) => {
  const cache = await db
    .collection("teamKpiData")
    .findOne({ _id: periodMetricUserAchievementId });

  console.log({ metricCached: cache.metricCached });

  const updatedMetricCached = Object.entries(cache.metricCached).reduce(
    (acc, [key]) => ({ ...acc, [key]: false }),
    {}
  );

  console.log({ updatedMetricCached });

  return db
    .collection("teamKpiData")
    .updateOne(
      { _id: periodMetricUserAchievementId },
      { $set: { metricCached: updatedMetricCached } }
    );
};

module.exports = (app, db) => {
  app.get("/", (req, res) => {
    res.status(200).send("Welcome to our restful API");
  });

  app.get("/tenants", async (req, res) => {
    const tenantsRaw = await findTenants(db);
    const tenants = tenantsRaw.map(t => ({
      label: t.name,
      value: t.tenantId.identifier
    }));
    console.log(`Sending ${tenants.length} tenants`);

    res.send(tenants);
  });

  app.get("/team/:tenantId", async (req, res) => {
    const { tenantId } = req.params;
    const teamsRaw = await findTeams(db, tenantId);
    const teams = teamsRaw.map(t => ({
      label: t.name,
      value: t.teamId.identifier
    }));

    console.log(`Sending ${teams.length} ${tenantId} teams`);

    res.send(teams);
  });

  app.get("/team-kpi-terms/:teamId", async (req, res) => {
    const { teamId } = req.params;
    const termsRaw = await findTeamKpiTerms(db, teamId);
    const terms = termsRaw.map(t => ({
      label: t.name,
      value: t
    }));

    console.log(`Sending ${terms.length} terms`);

    res.send(terms);
  });

  app.get(
    "/team-kpi-cache/:periodMetricUserAchievementId/clear",
    async (req, res) => {
      const { periodMetricUserAchievementId } = req.params;

      const rawCache = await clearTeamTermCache(
        db,
        periodMetricUserAchievementId
      );

      res.send(rawCache);
    }
  );

  return app;
};
