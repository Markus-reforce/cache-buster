import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Auth from "./auth";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

const auth = new Auth();

ReactDOM.render(<App auth={auth} />, document.getElementById("root"));
registerServiceWorker();
