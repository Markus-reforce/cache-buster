import React, { Component } from "react";
import Select from "react-select";
import "./App.css";
import history from "./history";
import logo from "./logo.svg";

const API_BASE = "https://cache-buster-api.appspot.com/";

const LOADING = true;

function handleSelect(currentType, nextType, nextUrl, selected) {
  this.setState(
    state => ({
      [currentType]: { ...state[currentType], selected },
      [nextType]: getInitial(LOADING),
      term: getInitial()
    }),
    () => {
      this.fetcher(nextUrl + selected.value)
        .then(list =>
          this.setState(
            {
              [nextType]: {
                ...getInitial(),
                list
              }
            },
            () => this[nextType].focus()
          )
        )
        .catch(e => console.log(`Failed to fetch ${nextType}s:`, e));
    }
  );
}

const getInitial = (isLoading = false) => ({
  list: [],
  isLoading,
  selected: null
});

class App extends Component {
  state = {
    tenant: getInitial(LOADING),
    team: getInitial(),
    term: getInitial(),
    loggedIn: this.props.auth.isAuthenticated()
  };

  componentDidMount() {
    if (/access_token|id_token|error/.test(history.location.hash)) {
      this.props.auth.handleAuthentication(
        err => {
          this.setState({
            loggedIn: false
          });
        },
        () => {
          this.setState({
            loggedIn: true
          });
          this.fetchTenants();
        }
      );
    } else if (this.state.loggedIn) {
      this.fetchTenants();
    }
  }

  fetchTenants() {
    this.fetcher = route =>
      fetch(API_BASE + route, {
        mode: "cors",
        headers: {
          authorization: "Bearer " + this.props.auth.getAccessToken()
        }
      }).then(res => res.json());

    this.fetcher("/tenants")
      .then(list => {
        this.setState({
          tenant: { list, isLoading: false, selected: null }
        });
      })
      .catch(e => console.log(e));
  }

  handleTenantSelect = handleSelect.bind(this, "tenant", "team", "/team/");

  handleTeamSelect = handleSelect.bind(
    this,
    "team",
    "term",
    "/team-kpi-terms/"
  );

  handleTermSelect = selected => {
    this.setState(
      state => ({ term: { ...state.term, selected } }),
      () => {
        this.queryRef.focus();
        document.getElementsByClassName("answer")[0].style.visibility =
          "hidden";
      }
    );
  };

  handleClear = () => {
    if (!this.state.term.selected) return;
    const periodMetricUserAchievementId = this.state.term.selected.value
      .periodMetricUserAchievement["$id"];
    this.fetcher(`/team-kpi-cache/${periodMetricUserAchievementId}/clear`).then(
      res => {
        console.log(res);
        document.getElementsByClassName("answer")[0].style.visibility =
          "visible";
      }
    );
  };

  render() {
    const { tenant, team, term, loggedIn } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          {!loggedIn && (
            <button className="auth-btn" onClick={this.login.bind(this)}>
              Log In
            </button>
          )}
          {loggedIn && (
            <button className="auth-btn" onClick={this.logout.bind(this)}>
              Log Out
            </button>
          )}
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Cache Buster</h1>
        </header>

        {!loggedIn && (
          <div className="main">You need to be logged in to proceed</div>
        )}
        {loggedIn && (
          <div className="main">
            <p className="App-intro">To get started, select a Tenant</p>
            <Select
              className="mb"
              options={tenant.list}
              ref={ref => {
                this.tenant = ref;
              }}
              value={tenant.selected}
              onChange={this.handleTenantSelect}
              placeholder={
                tenant.isLoading ? "Loading Tenants..." : "Select Tenant"
              }
            />
            {tenant.selected && (
              <Select
                className="mb"
                options={team.list}
                ref={ref => {
                  this.team = ref;
                }}
                value={team.selected}
                onChange={this.handleTeamSelect}
                placeholder={
                  team.isLoading ? "Loading Teams..." : "Select Team"
                }
              />
            )}
            {team.selected && (
              <Select
                className="mb"
                options={term.list}
                ref={ref => {
                  this.term = ref;
                }}
                value={term.selected}
                onChange={this.handleTermSelect}
                placeholder={
                  term.isLoading ? "Loading Terms..." : "Select Term"
                }
              />
            )}
            {term.selected && (
              <p>
                <b>
                  {term.selected.value.name}
                  :&nbsp;
                </b>
                <input
                  style={{
                    width: "100%",
                    fontSize: "16px",
                    padding: ".5em",
                    boxSizing: "border-box"
                  }}
                  ref={ref => (this.queryRef = ref)}
                  readOnly
                  onFocus={e => e.currentTarget.select()}
                  value={`db.teamKpiData.find({"_id": "${
                    term.selected.value.periodMetricUserAchievement["$id"]
                  }"})`}
                />
                <div>
                  <div className="answer">Cache has been cleared</div>
                  <button className="action-btn" onClick={this.handleClear}>
                    Clear
                  </button>
                </div>
              </p>
            )}
          </div>
        )}
      </div>
    );
  }

  goTo(route) {
    this.props.history.replace(`/${route}`);
  }

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
    this.setState({ loggedIn: false });
  }
}

export default App;
