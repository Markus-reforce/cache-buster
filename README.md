# Getting Started

## Prerequisites

### Backend

The environment configuration is controlled via an `.env` file - in `/backend/` - containing which host url, database, username, and password to use when connection to the database.

So if no such file exists in the root folder of this project, create it:

```
touch .env
```

and populate the file with the following variables:
`DB_USER`,`DB_PASS`,`DB_HOST`,`DB_NAME`

Example:

```
# .env

DB_USER=reforce
DB_PASS=<CAN BE FOUND IN 1PASSWORD>
DB_HOST=mongodb61118-reexecute-production2.jelastic.elastx.net
DB_NAME=fasthi
```

## Install dependencies

Before you can start you need to install all dependencies.
It's the same process for the backend and the client.
First make sure you're in the correct directory (`/backend` or `/client`) and then run:

```
npm install
```

## Start the server

Also the same process for the backend and the dev-server for the client client.
First make sure you're in the correct directory (`/backend` or `/client`) and then run:

```
npm start
```

# Deploying

## Client

### Prerequisites

Firebase CLI tools

```
npm install -g firebase-tools
```

### Deploy

First build the client app:

```
npm run build
```

Then deploy it:

```
firebase deploy
```

## Backend

```
gcloud app deploy
```
